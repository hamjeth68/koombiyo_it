<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h3>Koombiyo IT</h3>
                    <p>
                        N0, 25 Epitamulla Rd, Sri Jayawardenepura Kotte 10100<br><br>
                        <strong>Phone:</strong>011-7111051<br>
                        <strong>Email:</strong>system@koombiyodelivery.com<br>
                    </p>
                </div>

                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>">Home</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>about">About us</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>services">Services</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>team">Our Team</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Our Services</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>services">Mobile Application</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>services">Web Applications</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>services">SEO</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>services">Software Development</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>services">Graphic Design</a></li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-6 footer-newsletter">
                    <h4>Join Our Koombiyo IT</h4>
                    <p>We are a software development team</p>
                    <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe">
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="container d-md-flex py-4">

        <div class="mr-md-auto text-center text-md-left">
            <div class="copyright">
                &copy; Copyright <strong><span>Koombiyo IT</span></strong>. All Rights Reserved
            </div>
            <!--        <div class="credits">
                       All the links in the footer should remain intact. 
                       You can delete the links only if you purchased the pro version. 
                       Licensing information: https://bootstrapmade.com/license/ 
                       Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/ 
                      Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                    </div>-->
        </div>
        <div class="social-links text-center text-md-right pt-3 pt-md-0">
            <a href="" class="twitter"><i class="bx bxl-twitter"></i></a>
            <a href="https://www.facebook.com/koombiyoit" class="facebook"><i class="bx bxl-facebook"></i></a>
            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div>
    </div>
</footer>
<!-- End Footer -->