
<!-- Vendor CSS Files -->
<link href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/vendor/animate.css/animate.min.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/vendor/remixicon/remixicon.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

<!-- Template Main CSS File -->
<link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="<?php echo base_url() ?>" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li><a href="<?php echo base_url() ?>home">Home</a></li>
                <li><a href="<?php echo base_url() ?>about">About</a></li>
                <li><a href="<?php echo base_url() ?>services">Services</a></li>
                
<!--                <li class="drop-down"><a href="">Drop Down</a>
                    <ul>
                        <li><a href="#">Drop Down 1</a></li>
                        <li class="drop-down"><a href="#">Deep Drop Down</a>
                            <ul>
                                <li><a href="#">Deep Drop Down 1</a></li>
                                <li><a href="#">Deep Drop Down 2</a></li>
                                <li><a href="#">Deep Drop Down 3</a></li>
                                <li><a href="#">Deep Drop Down 4</a></li>
                                <li><a href="#">Deep Drop Down 5</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Drop Down 2</a></li>
                        <li><a href="#">Drop Down 3</a></li>
                        <li><a href="#">Drop Down 4</a></li>
                    </ul>
                </li>-->
                <li><a href="<?php echo base_url() ?>home">Contact</a></li>

            </ul>
        </nav><!-- .nav-menu -->

    </div>
</header>
<!-- End Header -->
