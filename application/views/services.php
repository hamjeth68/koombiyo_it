<html>
    <head>
        <link href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/animate.css/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/remixicon/remixicon.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        
        <?php $this->load->view("include/head"); ?>
        <?php $this->load->view("include/header"); ?> 
        
    </head>

    <body>
        <!-- ======= Services Section ======= -->
        <section id="services" class="services">
            <div class="container">
                <div class="section-title">
                    <h2>Services</h2>
                    <p>Business services is a general term that describes work that supports a business but does not produce a tangible commodity..</p>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-brand-android-robot"></i></div>
                            <h4><a href="">Mobile Application</a></h4>
                            <p>As a organization we deliver quality applications</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-file-code"></i></div>
                            <h4><a href="">Web Applications</a></h4>
                            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-gears"></i></div>
                            <h4><a href="">SEO</a></h4>
                            <p>Search engine optimization (SEO) is the process of optimizing your online content so that a search engine</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-color-bucket"></i></div>
                            <h4><a href="">Graphic design</a></h4>
                            <p>Graphic design is a craft where professionals create visual content to communicate messages.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-code"></i></div>
                            <h4><a href="">Software Development</a></h4>
                            <p>Software development is the process of conceiving, specifying, designing, programming, documenting, testing, and bug fixing involved in creating and maintaining applications, frameworks</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-handshake-deal"></i></div>
                            <h4><a href="">Marketing</a></h4>
                            <p>Marketing management is the organizational discipline which focuses on the practical application of marketing orientation</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Services Section -->
        
        <div id="preloader"></div>
        <a href="<?php echo base_url() ?>services" class="back-to-top"><i class="icofont-simple-up"></i></a>
      

        <?php $this->load->view("include/footer"); ?>

        <!-- Vendor JS Files -->
        <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/php-email-form/validate.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/venobox/venobox.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/waypoints/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/counterup/counterup.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

        <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    </body>
</html>