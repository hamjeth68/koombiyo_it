<html>
    <head>
        <link href="assets/img/favicon.png" rel="icon">
        <link href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/animate.css/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/remixicon/remixicon.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

        <?php $this->load->view("include/header"); ?>
        <?php $this->load->view("include/head"); ?>

    </head>

    <body>
        <!-- ======= About Section ======= -->
        <section id="about" class="about">
            <div class="container-fluid">

                <div class="section-title">
                    <h2>About</h2>
                </div>

                <div class="row">
                    <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch">
                        <a href="https://www.youtube.com/watch?v=xVZodS_S8bo" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                    </div>

                    <div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                        <h3>We are a software development organization</h3>
                        <p>Software companies are part of the broader computer industry and provide products in one of four categories: programming services, system services, open source, and software as a service (SaaS). ... Software companies are also some of the world's top developers of enterprise solutions.</p>

                        <div class="icon-box">
                            <div class="icon"><i class="icofont-chart-histogram-alt"></i></div>
                            <h4 class="title"><a href="<?php echo base_url() ?>services">Business Development Solutions</a></h4>
                            <p class="description">Our unique data-driven, human-led, and technology-powered approach creates the trusted, actionable, and forward-looking intelligence you need to make faster, more informed decisions.</p>
                        </div>

                        <div class="icon-box">
                            <div class="icon"><i class="icofont-console"></i></div>
                            <h4 class="title"><a href="<?php echo base_url() ?>services">Software Solutions</a></h4>
                            <p class="description">All types of software solutions in your fingertips</p>
                        </div>

                        <div class="icon-box">
                            <div class="icon"><i class="bx bx-atom"></i></div>
                            <h4 class="title"><a href="<?php echo base_url() ?>services">software consultancy</a></h4>
                            <p class="description">The providing of expert knowledge in the software space to a third party for a fee. Software consulting is most often used when a company needs an outside, expert opinion regarding a business decision. ... Working for a 'software consulting' company is not software consulting.</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ======= Departments Section ======= -->
            <section id="departments" class="departments">
                <div class="container">

                    <div class="section-title">
                        <h2>Departments</h2>
                        <p>Evalution of Koombiyo Departments</p>
                    </div>

                    <div class="row">
                        <div class="col-lg-3">
                            <ul class="nav nav-tabs flex-column">
                                <li class="nav-item">
                                    <a class="nav-link active show" data-toggle="tab" href="#tab-1">Mobile Application</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-2">Web Applications</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-3">SEO</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-4">Graphic design</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-5">Software development</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-9 mt-4 mt-lg-0">
                            <div class="tab-content">
                                <div class="tab-pane active show" id="tab-1">
                                    <div class="row">
                                        <div class="col-lg-8 details order-2 order-lg-1">
                                            <h3>Information Technology</h3>
                                            <p class="font-italic">Information technology is the study, design, development, implementation, support or management of computer-based information systems—particularly software applications and computer hardware.</p>
                                            <p>Someone with an IT job would most commonly work for a company in their Information Technology department.</p>
                                        </div>
                                        <div class="col-lg-4 text-center order-1 order-lg-2">
                                            <img src="assets/img/departments-1.png" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-2">
                                    <div class="row">
                                        <div class="col-lg-8 details order-2 order-lg-1">
                                            <h3>Accounts</h3>
                                            <p class="font-italic">In accounting, an account is simply a unique place in the general ledger that is used for recording a dollar balance along with a history of changes to that balance</p>
                                            <p>This balance can be associated with a bank account or it can represent the amount of money owed to you by a client or customer.</p>
                                        </div>
                                        <div class="col-lg-4 text-center order-1 order-lg-2">
                                            <img src="assets/img/departments-2.png" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-3">
                                    <div class="row">
                                        <div class="col-lg-8 details order-2 order-lg-1">
                                            <h3>Human Resource</h3>
                                            <p class="font-italic">Human resources is the set of the people who make up the workforce of an organization, business sector, industry, or economy.</p>
                                            <p>A narrower concept is human capital, the knowledge which the individuals embody. Similar terms include manpower, labor, personnel, associates or simply people.</p>
                                        </div>
                                        <div class="col-lg-4 text-center order-1 order-lg-2">
                                            <img src="assets/img/departments-3.png" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-4">
                                    <div class="row">
                                        <div class="col-lg-8 details order-2 order-lg-1">
                                            <h3>Liability Management</h3>
                                            <p class="font-italic">Liability management is the practice by banks of maintaining a balance between the maturities of their assets and their liabilities</p>
                                            <p>Asset Liability Management (ALM) can be defined as a mechanism to address the risk faced by a bank due to a mismatch between assets and liabilities either due to liquidity or changes in interest rates.</p>
                                        </div>
                                        <div class="col-lg-4 text-center order-1 order-lg-2">
                                            <img src="assets/img/departments-4.png" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-5">
                                    <div class="row">
                                        <div class="col-lg-8 details order-2 order-lg-1">
                                            <h3>Customer Care</h3>
                                            <p class="font-italic">Customer care is the process of building an emotional connection </p>
                                            <p>Customer care is the process of looking after customers to best ensure their satisfaction and delightful interaction with a business and its brand</p>
                                        </div>
                                        <div class="col-lg-4 text-center order-1 order-lg-2">
                                            <img src="assets/img/departments-5.png" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- End Departments Section -->
        </section>
        <!-- End About Section -->

<!--         ======= Team Section ======= -->
        <section id="doctors" class="doctors">
            <div class="container">
                
                <div class="section-title">
                    <h2>Our Team</h2>
                    <p>Developers or software engineers are team members that apply their knowledge of engineering and programming</p>
                </div>

                <div class="row">

                    <div class="col-lg-6">
                        <div class="member d-flex align-items-start">
                            <div class="pic"><img src="assets/img/doctors/user.jpg" class="img-fluid" alt=""></div>
                            <div class="member-info">
                                <h4>Mr.Sajith</h4>
                                <span>Front-End Engineer</span>
                                <p>Explicabo voluptatem mollitia et repellat qui dolorum quasi</p>
                                <div class="social">
                                    <a href=""><i class="ri-twitter-fill"></i></a>
                                    <a href="https://www.facebook.com/koombiyoit"><i class="ri-facebook-fill"></i></a>
                                    <a href=""><i class="ri-instagram-fill"></i></a>
                                    <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 mt-4 mt-lg-0">
                        <div class="member d-flex align-items-start">
                            <div class="pic"><img src="assets/img/doctors/user.jpg" class="img-fluid" alt=""></div>
                            <div class="member-info">
                                <h4>Mr.Nadun</h4>
                                <span>Back-End Engineer</span>
                                <p>Aut maiores voluptates amet et quis praesentium qui senda para</p>
                                <div class="social">
                                    <a href=""><i class="ri-twitter-fill"></i></a>
                                    <a href="https://www.facebook.com/koombiyoit"><i class="ri-facebook-fill"></i></a>
                                    <a href=""><i class="ri-instagram-fill"></i></a>
                                    <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 mt-4">
                        <div class="member d-flex align-items-start">
                            <div class="pic"><img src="assets/img/doctors/user.jpg" class="img-fluid" alt=""></div>
                            <div class="member-info">
                                <h4>Mr.Hamjeth</h4>
                                <span>Front-End Engineer</span>
                                <p>Quisquam facilis cum velit laborum corrupti fuga rerum quia</p>
                                <div class="social">
                                    <a href=""><i class="ri-twitter-fill"></i></a>
                                    <a href="https://www.facebook.com/koombiyoit"><i class="ri-facebook-fill"></i></a>
                                    <a href=""><i class="ri-instagram-fill"></i></a>
                                    <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 mt-4">
                        <div class="member d-flex align-items-start">
                            <div class="pic"><img src="assets/img/doctors/user.jpg" class="img-fluid" alt=""></div>
                            <div class="member-info">
                                <h4>Amanda Jepson</h4>
                                <span>SEO</span>
                                <p>Dolorum tempora officiis odit laborum officiis et et accusamus</p>
                                <div class="social">
                                    <a href=""><i class="ri-twitter-fill"></i></a>
                                    <a href="https://www.facebook.com/koombiyoit"><i class="ri-facebook-fill"></i></a>
                                    <a href=""><i class="ri-instagram-fill"></i></a>
                                    <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<!--         End Doctors Section -->
        
<!--        back to top button -->
        <div id="preloader"></div>
        <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

        <!--        Vendor JS Files -->
        <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/php-email-form/validate.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/venobox/venobox.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/waypoints/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/counterup/counterup.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url() ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

        <script src="<?php echo base_url() ?>assets/js/main.js"></script>

        <?php $this->load->view("include/footer"); ?>
    </body>
</html>