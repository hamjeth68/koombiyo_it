<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

    var $title = "Koombiyo Super";
    var $pagen = "Services";
    var $navi_active = 'services';

    public function index() {
        $data['title'] = $this->title;
        $data['pagen'] = $this->pagen;
        $data['navi_active'] = $this->navi_active;
//         $data['navi_active'] = 'services';
        $this->load->view('services', $data);
    }
}