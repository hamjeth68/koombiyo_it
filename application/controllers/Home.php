<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    var $title = "Koombiyo Super";
    var $pagen = "Home";
    var $navi_active = 'home';

    public function index() {
        $data['title'] = $this->title;
        $data['pagen'] = $this->pagen;
        $data['navi_active'] = $this->navi_active;
         $data['navi_active'] = 'home';
        $this->load->view('home', $data);
    }
}
