<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    var $title = "Koombiyo IT";
    var $pagen = "About";
    var $navi_active = 'about';

    public function index() {
        $data['title'] = $this->title;
        $data['pagen'] = $this->pagen;
        $data['navi_active'] = $this->navi_active;
         $data['navi_active'] = 'about';
        $this->load->view('about', $data);
    }
}

